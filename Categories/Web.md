<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## WEB BROWSER


* [BeHe ExploreR](http://forum.xda-developers.com/android/apps-games/app4-0-behe-explorer-internet-browser-t3313025): BeHe ExploreR it's a simple,small and minimalistic internet browser made for experimenting and learning.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/VladThodo/behe-explorer)  
[![](Pictures/F-Droid.png)](https://f-droid.org/app/com.vlath.beheexplorer)

***

* [Brave / Link Bubble](http://v.ht/dmwh): The new Brave browser automatically blocks ads and trackers, making it faster and safer than your current browser.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)(to be confirmed. That's the license used for the desktop version. No available information for the Android app)  
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](http://v.ht/sdTe)  
[![](Pictures/Google_Play.png)](http://v.ht/dmwh)  
_Non-free crashlytics_

***

* [Browser](https://f-droid.org/repository/browse/?fdfilter=browser&fdid=de.baumann.browser): Original Material browser based on webview.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/scoute-dich/browser/)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=browser&fdid=de.baumann.browser)

***

* [Code Aurora SWE Browser](https://forum.xda-developers.com/android/apps-games/app-code-aurora-swe-browser-m55-t3523365): Fully stock and unchanged Code Aurora Snapdragon Optimized Browser.
![](https://img.shields.io/badge/License-Various-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://source.codeaurora.org/quic/chrome4sdp/)  
[![](Pictures/3rd-party.png)](https://forum.xda-developers.com/android/apps-games/app-code-aurora-swe-browser-m55-t3523365)  
[Licenses](https://wiki.codeaurora.org/xwiki/bin/Chromium+for+Snapdragon/#HLicenses)

***

* [Cornowser](http://v.ht/WPjP): based on Chromium/Crosswalk. It is intended to be a fast, modern and inituitive browser. Ad-free.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/xdevs23/Cornowser)  
[![](Pictures/3rd-party.png)](https://raw.githubusercontent.com/xdevs23/Cornowser/master/update/Cornowser.apk)

***

* [Firefox](http://v.ht/7TI2): (still maintained on F-Droid): Mobile version of the Firefox web browser. Uses the Gecko layout engine to render web pages, which implements current and anticipated web standards. Features include: bookmark sync, custom search engines, support for addons and the ability to enable or disable the search suggestions.  
:bulb: Learn how to tweak the built-in option from about.config through a user.js config file.
![MPL2](https://img.shields.io/badge/License-MPL2-yellow.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Mozilla-lightgrey.svg?style=flat-square)](http://hg.mozilla.org/)  
[![](Pictures/F-Droid.png)](http://v.ht/7TI2)  
_Non-Free Addons - Tracks You - Upstream Non-Free_

***

* [getChromium](http://v.ht/ETWs): Helps to install/update the open-source [Chromium browser](http://v.ht/9muz/).
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/andDevW/getChromium)  
[![](Pictures/F-Droid.png)](http://v.ht/ETWs)  

Many Chromium based browsers are available on XDA:
1. [arter97's custom-built Chromium for Snapdragon devices](http://forum.xda-developers.com/android/apps-games/arter97-s-custom-built-chromium-t3477758)
2. [Pyrope Browser](http://forum.xda-developers.com/android/apps-games/app-gello-browser-t3384782)
3. [#NoChromo](http://forum.xda-developers.com/android/apps-games/app-nochromo-wild-browser-appears-t3130776)
4. [My unofficial Chromium builds -ARM/X86-](http://forum.xda-developers.com/android/apps-games/unofficial-chromium-builds-arm-x86-t3355105)
5. You can also find new ones on Google Play (e.g. [YuBrowser](https://play.google.com/store/apps/details?id=com.mokee.yubrowser)).

***

* [IceCatMobile](http://v.ht/4i1D): Browser using the Gecko layout engine to render web pages, which implements current and anticipated web standards. This is a free software rebranding of the latest Firefox-ESR release.
![MPL2](https://img.shields.io/badge/License-MPL2-yellow.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Mozilla-lightgrey.svg?style=flat-square)](http://hg.mozilla.org/)  
[![](Pictures/F-Droid.png)](http://v.ht/4i1D)

***

* [JumpGo](http://v.ht/bxGD): A fast, reliable and easy to use way to browse the web! Based on Lightning.
![MPL2](https://img.shields.io/badge/License-MPL2-yellow.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/JTechMe/JumpGo)  
[![](Pictures/F-Droid.png)](http://v.ht/bxGD)

***

* [Lightning](http://v.ht/7YD1): Lightweight browser.
![MPL2](https://img.shields.io/badge/License-MPL2-yellow.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/anthonycr/Lightning-Browser)  
[![](Pictures/F-Droid.png)](http://v.ht/7YD1)

***

* [Lucid browser](https://play.google.com/store/apps/details?id=com.powerpoint45.lucidbrowser): Free and open source browser designed to be small, light, fast, and simple.
![MPL2](https://img.shields.io/badge/License-MPL2-yellow.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/powerpoint45/Lucid-Browser)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=com.powerpoint45.lucidbrowser)  
_Non-free dependencies: Play Services_

***

* [mBrowser](http://v.ht/VKoq): Simple webview based browser.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/chelovek84/mBrowser)  
[![](Pictures/F-Droid.png)](http://v.ht/VKoq)

***

* [Orfox](http://v.ht/Iljf) / [Orbot](https://f-droid.org/repository/browse/?fdfilter=orbot&fdid=org.torproject.android): Tor is both software and an open network that helps you defend against network surveillance that threatens personal freedom and privacy, confidential business activities and relationships.
![NewBSD](https://img.shields.io/badge/License-NewBSD-25B3D6.svg?style=flat-square)
[![](https://img.shields.io/badge/Source OrFox-Github-lightgrey.svg?style=flat-square)](https://github.com/guardianproject/Orfox) [![](https://img.shields.io/badge/Source Orbot-Tor Project-lightgrey.svg?style=flat-square)](https://gitweb.torproject.org/orbot.git)  
[![](Pictures/Google_Play.png)](http://v.ht/Iljf) [![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=orbot&fdid=org.torproject.android)  

***

* [Privacy Browser](http://v.ht/E8mm): A web browser that protects your privacy by giving you easy control over the use of JavaScript, DOM storage, and cookies. Many websites use these technologies to track users between visits and between sites. Tapping the privacy shield toggles whether JavaScript is enabled and reloads the WebView. When JavaScript is disabled, not only are privacy and security increased, but web pages load faster with less bandwidth consumption.
![GPLv3+](https://img.shields.io/badge/License-GPLv3+-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Git Stoutner-lightgrey.svg?style=flat-square)](https://git.stoutner.com/?p=PrivacyBrowser.git;a=summary)  
[![](Pictures/F-Droid.png)](http://v.ht/E8mm)

***

* [WebApps](http://v.ht/Du3R): Provide a secure way to browse popular webapps by eliminating referrers, 3rd party requests, cookies, cross-site scripting, etc.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/tobykurien/webapps)  
[![](Pictures/F-Droid.png)](http://v.ht/Du3R)