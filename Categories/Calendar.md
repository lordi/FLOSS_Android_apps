<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## CALENDARS / CONTACTS

* [Calendar](http://v.ht/LHAu): A simple calendar with events and a customizable widget.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SimpleMobileTools/Simple-Calendar)  
[![](Pictures/F-Droid.png)](http://v.ht/LHAu)

***

* [Calendar Import-Export](http://v.ht/sRMe): Import/Export calendars via ics files.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SufficientlySecure/calendar-import-export)  
[![](Pictures/F-Droid.png)](http://v.ht/sRMe)

***

* [DAVdroid](http://v.ht/pkUE): CalDAV/CardDAV synchronisation adapter for Android 4+ devices.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-davdroid-lightgrey.svg?style=flat-square)](https://davdroid.bitfire.at/source/)  
[![](Pictures/F-Droid.png)](http://v.ht/pkUE)

***

* [Etar](http://v.ht/IDXQ): Etar is material designed calendar based on the AOSP calendar.  
**Note**: go to [this website](https://www.mozilla.org/en-US/projects/calendar/holidays/) to download your nation’s holidays.
![Apache2](https://img.shields.io/badge/License-Apache%202.0-yellowgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/xsoh/Etar-Calendar)  
[![](Pictures/F-Droid.png)](http://v.ht/IDXQ)

***

* [EteSync](https://f-droid.org/repository/browse/?fdid=com.etesync.syncadapter): Synchronize contacts and calendars securely.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/etesync/android)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=com.etesync.syncadapter)

***

* [Import contacts](http://v.ht/zyRr) / [Export contacts](http://v.ht/I456): Import / Export your contacts directly from your Android device without the need to upload them to Google first.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source Import-bzr.ed.am-lightgrey.svg?style=flat-square)](http://bzr.ed.am/android/import-contacts)
[![](https://img.shields.io/badge/Source Export-bzr.ed.am-lightgrey.svg?style=flat-square)](http://bzr.ed.am/android/export-contacts)  
[![](Pictures/F-Droid.png)](http://v.ht/zyRr)  [![](Pictures/F-Droid.png)](http://v.ht/I456)

***

* [Offline calendar](http://v.ht/B2WP): Lets you add offline calendars to the Calendar app, which are not synchronized and are accessible only on the device itself.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/SufficientlySecure/offline-calendar)  
[![](Pictures/F-Droid.png)](http://v.ht/B2WP)