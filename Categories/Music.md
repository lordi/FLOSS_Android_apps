<!--
    Copyright (C)  2016 PRIMOKORN.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
-->
## MUSIC and AUDIO

* [Audinaut](https://f-droid.org/repository/browse/?fdfilter=Audinaut&fdid=net.nullsum.audinaut): Connect to a libresonic instance and access your media.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/nvllsvm/Audinaut)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdfilter=Audinaut&fdid=net.nullsum.audinaut)

***

* [Lyrically](http://v.ht/Qjqc): An everywhere lyrics app which can be used by simply swiping a part of your screen. See the lyrics without interrupting what you're doing.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/shkcodes/Lyrically)  
[![](Pictures/F-Droid.png)](http://v.ht/Qjqc)

***

* [Omnirom DSP Manager FOR All other roms](http://v.ht/19sd): Modded apk from Omnirom.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Website/Source-Github-lightgrey.svg?style=flat-square)](https://james34602.github.io/JamesDSPManager/)  
[![](Pictures/3rd-party.png)](https://github.com/james34602/JamesDSPManager/releases)

***

* [Play Music Exporter](https://f-droid.org/repository/browse/?fdid=re.jcg.playmusicexporter): This app allows you to export your music from Play Music onto your internal storage, or even some DocumentsProvider.
![MIT](https://img.shields.io/badge/License-MIT-orange.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/jcgruenhage/PlayMusicExporter)  
[![](Pictures/F-Droid.png)](https://f-droid.org/repository/browse/?fdid=re.jcg.playmusicexporter)

***

* [RadioDroid](http://v.ht/xoOG1): Browse and stream internet radios.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/segler-alex/RadioDroid)  
[![](Pictures/F-Droid.png)](http://v.ht/xoOG1)

***

* [Transcribe](https://github.com/p-edelman/Transcribe): A tool for people who need to transcribe audio recordings to text (more specifically, people that are slow typists and have sticky fingers.
![GPLv3](https://img.shields.io/badge/License-GPLv3-brightgreen.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/p-edelman/Transcribe)  
[![](Pictures/Google_Play.png)](https://play.google.com/store/apps/details?id=org.mrpi.transcribe)

***

* [ViPER4Android FX](http://v.ht/T2LF): Popular audio mod.
![](https://img.shields.io/badge/License-Missing-000000.svg?style=flat-square)
[![](https://img.shields.io/badge/Source-Github-lightgrey.svg?style=flat-square)](https://github.com/vipersaudio/viper4android_fx)  
[![](Pictures/3rd-party.png)](http://v.ht/T2LF)  
([Universal Fix SELinux Enforcing 6.0+](http://v.ht/zzjZ))  
Up to Android 5. The source code of newer versions is not available. Forks are not open source (ViPER4Arise, ViPERFX...)